Nutanix VM
=========

This role creates vm's on a Nutanix cluster utilizing the Nutanix API's.

Requirements
------------

Nutanix Cluster

Role Variables
--------------

```yaml
---
# defaults file for nutanix-vm

# Following user used for connecting to API and any ncli commands are executed using this user as well.
cluster_api_host: FQDN of CVM or Prism Central

ntnx_api_user: "{{vault_ntnx_api_user}}"  # User for connecting to Nutanix API
ntnx_api_pass: "{{vault_ntnx_api_pass}}"  # Password for connecting to Nutanix API

# templates/unattend_xml.j2 specific variables

local_admin_pass: "{{vault_local_admin_pass}}"  # Local admin password, set in the templates/unattend_xml.j2
registered_organization: Company Name
registered_owner: Your Name
timezone: Pacific Standard Time

input_local: en-CA
system_local: en-US
ui_lang: en-US
user_locale: en-CA

# templates/cloud-init.j2 specific variables

cloud_init_user: ansible      # Will create this user for use with Ansible
ssh_key: "{{vault_ssh_key}}"  # Copy of SSH public key for passwordless login

```


Dependencies
------------

None specifically

Tested on AOS version 5.8.1

Example Playbook
----------------

Example vm creation for a Windows based OS, using an unattend file.

```yaml
- hosts: server
  gather_facts: no
  tasks:
  - import_role:
      name: nutanix-vm
    vars:
      guest_customization: sysprep
      guest_customization_type: unattend_xml
      guest_customization_template: unattend_xml.j2
```

Example of a vm creation for a Linux based OS, using a cloud-init config.
```yaml
- hosts: server
  gather_facts: no
  tasks:
  - import_role:
      name: nutanix-vm
    vars:
      guest_customization: cloud-init
      guest_customization_type: user_data
      guest_customization_template: cloud-init.j2
```

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
